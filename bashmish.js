var BashMish = (function() {

  // Class attribute manipulation functions
  // (taken from http://websemantics.co.uk/resources/useful_javascript_functions/)
  function hasClass(o,c){return new RegExp('(\\s|^)'+c+'(\\s|$)').test(o.className);}
  function addClass(o,c){if(!hasClass(o,c)){o.className+=' '+c;}}
  function removeClass(o,c){if(hasClass(o,c)){o.className=o.className.replace(new RegExp('(\\s|^)'+c+'(\\s|$)'),' ').replace(/\s+/g,' ').replace(/^\s|\s$/,'');}}

  // Function to retrive elements styles
  // (taken from http://robertnyman.com/2006/04/24/get-the-rendered-style-of-an-element/)
  function getStyle(oElm, strCssRule){
    var strValue = '';
    if(document.defaultView && document.defaultView.getComputedStyle){
      strValue = document.defaultView.getComputedStyle(oElm, '').getPropertyValue(strCssRule);
    }
    else if(oElm.currentStyle){
      strCssRule = strCssRule.replace(/\-(\w)/g, function (strMatch, p1){
        return p1.toUpperCase();
      });
      strValue = oElm.currentStyle[strCssRule];
    }
    return strValue;
  }

  return {

    // Input tip for input and textarea elements.
    // Shows default text for element when nothing is entered in it.
    inputTip: function (inputs)
    {
      for (var i = 0, n1 = inputs.length; i < n1; i++)
      {
        tip = inputs[i][0];
        (function(tip) {
          elements = inputs[i][1];
          for (var j = 0, n2 = elements.length; j < n2; j++)
          {
            e = elements[j];
            if (e.value == '' || e.value == tip) {
              e.value = tip;
            } else {
              addClass(e, 'active');
            }
            e.onfocus = function()
            {
              if (this.value == tip)
              {
                this.value = '';
                addClass(this, 'active');
              }
            }
            e.onblur = function()
            {
              if (this.value == '')
              {
                this.value = tip;
                removeClass(this, 'active');
              }
            }
          }
        })(tip);
      }
    },

    // Pretty navigation.
    // Navigation elements adjusted along container width.
    prettyNav: function (container)
    {
      container_pl = parseInt(getStyle(container, 'padding-left'));
      container_pr = parseInt(getStyle(container, 'padding-right'));
      container_width = container.clientWidth - container_pl - container_pr;

      child_nodes = container.childNodes;
      children = new Array();
      children_n = 0;
      for (var i = 0; i < child_nodes.length; i++)
      {
        if (child_nodes[i].nodeType == 1)
        {
          children[children_n] = child_nodes[i];
          children_n++;
        }
      }

      children_width = 0;
      for (var i = 0; i < children_n; i++)
      {
        children_width += children[i].offsetWidth;
      }

      gaps_n = children_n - 1;
      gap_width = Math.floor((container_width - children_width)/gaps_n);
      for (var i = 0; i < gaps_n; i++)
      {
        children[i].style.marginRight = gap_width + 'px';
      }
      children[gaps_n].style.marginRight = 0 + 'px';
    }
  };

})();
